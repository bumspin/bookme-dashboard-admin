// JavaScript Document

(function(){
	"use strict";
	
	var filterSelect = $(".filter-select"),
	  	filterInput = $(".f-input"),
		moreInfo = $(".more-info"),
		infoList = $(".info-list");

	filterSelect.on('change', function () {
	  filterInput.attr('placeholder', 'Filter by ' + filterSelect.find(':selected').text());
	});
	
	moreInfo.on('click', function(){
		infoList.fadeToggle();
	});
})();